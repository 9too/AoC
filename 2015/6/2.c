#include <stdio.h>
#include <string.h>

#define X 1000
#define Y 1000

typedef enum {ON,OFF,TOGGLE} Switch;

void main() {
    Switch light;
    int x1,y1, x2,y2, i,j, count = 0;
    char op[8], grid[X][Y] = {0};
    while(EOF != scanf("%s",op)) {
        if (!strcmp(op,"on"))
            light = ON;
        else if (!strcmp(op,"off"))
            light = OFF;
        else if (!strcmp(op,"toggle"))
            light = TOGGLE;
        else continue;
        scanf("%i,%i through %i,%i", &x1,&y1, &x2,&y2);
        switch (light) {
            case ON:
                for (i=x1; i<=x2; ++i)
                    for (j=y1; j<=y2; ++j)
                        grid[i][j]++;
                break;
            case OFF:
                for (i=x1; i<=x2; ++i)
                    for (j=y1; j<=y2; ++j)
                        if(0 < grid[i][j])
                            grid[i][j]--;
                break;
            case TOGGLE:
                for (i=x1; i<=x2; ++i)
                    for (j=y1; j<=y2; ++j)
                        grid[i][j] += 2;
                break;
        }
    }
    for (x1=0; x1<X; ++x1)
        for (y1=0; y1<Y; ++y1)
            count += grid[x1][y1];
    printf("%i\n",count);
}
