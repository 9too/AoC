import Control.Arrow
import Data.Maybe
import Data.List
import Data.List.Split
import Data.Tuple
import qualified Data.Map.Strict as Map

type Location = String

parse :: String -> Map.Map (Location,Location) Int
parse input = let transform [x,_,y,_,z] = ((x, y), read z)
                  transform x = error $ "bad input" ++ show x
               in Map.fromList
                  $ map (transform . splitOn " ")
                  $ filter (not . null)
                  $ splitOn "\n" input

evaluate :: Map.Map (Location,Location) Int -> [Location] -> Maybe Int
evaluate _ [_] = Just 0
evaluate paths path = let from:trail = path
                          to         = head trail
                          distance   = Map.lookup (from,to) paths
                       in sum <$> sequence [distance, evaluate paths trail]

main :: IO ()
main = do
    input <- getContents
    let oneway = parse input 
        twoway = Map.union oneway $ Map.mapKeys swap oneway
        list   = nub $ concat [ [x,y] | ((x,y),_) <- Map.toList oneway]
     in print
        $ minimum &&& maximum
        $ map fromJust
        $ filter isJust
        $ map (evaluate twoway)
        $ permutations list
