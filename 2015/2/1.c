#include <stdio.h>

void main() {
    int l,w,h,
        s1,s2,s3,
        total=0;
    while(EOF != scanf("%ix%ix%i",&l,&w,&h)) {
        s1=l*w; s2=w*h; s3=h*l;
        total += 2*(s1+s2+s3) + (s1<s2 ? (s1<s3 ? s1:s3):(s2<s3 ? s2:s3));
    }
    printf("%i\n",total);
}
