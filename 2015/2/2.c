#include <stdio.h>

void main() {
    int l,w,h, s1,s2, total=0;
    while(EOF != scanf("%ix%ix%i",&l,&w,&h)) {
        if (l < h) {
            s1 = l;
            s2 = w<h ? w:h;
        } else {
            s1 = h;
            s2 = w<l ? w:l;
        }
        total += 2*(s1+s2)+l*w*h;
    }
    printf("%i\n",total);
}
