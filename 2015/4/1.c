#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <bsd/md5.h>

void main() {
    unsigned int i;
    char in[64], out[64];
    scanf("%s",in);
    for (i=0; ++i;) {
        sprintf(out,"%s%i",in,i);
        char *sum = MD5Data(out, strlen(out), NULL);
        if (!strncmp(sum,"00000",5)) break;
        free(sum);
    }
    printf("%i\n",i);
}
