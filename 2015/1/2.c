#include <stdio.h>

void main() {
    char c;
    int f=0, p=1;
    while(EOF != (c = getchar())) {
        switch(c) {
            case '(': ++f; break;
            case ')': --f; break;
        }
        if (f == -1) {
            printf("%i\n",p);
            return;
        }
        ++p;
    }
}
