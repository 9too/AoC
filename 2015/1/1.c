#include <stdio.h>

void main() {
    char c;
    int f=0;
    while(EOF != (c = getchar()))
        switch(c) {
            case '(': ++f; break;
            case ')': --f; break;
        }
    printf("%i\n",f);
}
