import System.Environment

main = do
    args   <- getArgs
    number <- getLine
    let iteration = read $ head args
    print $ length $ iterate say number !! iteration

look :: String -> (Int, Char, String)
look [x]    = (1, x, "")
look (x:xs) = if x == head xs
              then (\(i,c,s) -> (i+1,c,s)).look $ xs
              else (1, x, xs)

say :: String -> String
say [] = ""
say s  = (\(i,c,s) -> show i ++ c:(say s)).look $ s
