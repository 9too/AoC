#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const int alpha_range = 'z' - 'a' + 1;

typedef char var[3];

typedef enum {
    NONE = 0,
    NOT,
    AND,
    OR,
    LSHIFT,
    RSHIFT
} gate;

typedef enum {
    CONSTANT = 0,
    VARIABLE
} input_type;

typedef union {
    long int constant;
    var variable;
} input;

typedef struct {
    input_type type;
    input value;
} parameter;

typedef struct {
    parameter in[2];
    gate gate;
    var out;
    bool resolved;
    int result;
} connector;

connector* new_connector() {
    const int size = sizeof(connector);
    connector *new = malloc(size);
    new->resolved = false;
    return memset(new, 0, size);
}

void print_connector(const connector *plug) {
    if (plug->in[0].type == CONSTANT)
        printf("%li", plug->in[0].value.constant);
    else
        printf("%s", plug->in[0].value.variable);
    switch(plug->gate) {
        case NONE:
            printf(" ");
            break;
        case NOT:
            printf(" NOT ");
            break;
        case AND:
            printf(" AND ");
            break;
        case OR:
            printf(" OR ");
            break;
        case LSHIFT:
            printf(" LSHIFT ");
            break;
        case RSHIFT:
            printf(" RSHIFT ");
            break;
    }
    if (plug->in[1].type == CONSTANT)
        printf("%li -> ", plug->in[1].value.constant);
    else
        printf("%s -> ", plug->in[1].value.variable);
    puts(plug->out);
}

bool match(const char *token, const char *value) {
    return !strcmp(token, value);
}

void read_parameter(const char *token, parameter *res) {
    char *end;
    long int number = strtol(token, &end, 10);
    if (*end == '\0') {
        res->value.constant = number;
        res->type = CONSTANT;
    }
    else {
        strcpy(res->value.variable, token);
        res->type = VARIABLE;
    }
}

gate read_gate(const char *token) {
    if (match("NOT", token))
        return NOT;
    if (match("AND", token))
        return AND;
    if (match("OR", token))
        return OR;
    if (match("LSHIFT", token))
        return LSHIFT;
    if (match("RSHIFT", token))
        return RSHIFT;
    return NONE;
}

int _(const char *variable) {
    int base = 1, pos = 0, result = -1;
    for (const char *it = variable; *it != '\0'; ++it) {
        result += (*it - 'a' + 1) * base;
        base *= alpha_range;
    }
    return result;
}

int compute(connector **plugs, const char *variable) {
    connector *plug = plugs[_(variable)];
    if (plug->resolved) return plug->result;
    int a = plug->in[0].type == CONSTANT
          ? plug->in[0].value.constant
          : compute(plugs, plug->in[0].value.variable);
    int b = plug->in[1].type == CONSTANT
          ? plug->in[1].value.constant
          : compute(plugs, plug->in[1].value.variable);
    switch(plug->gate) {
        case NONE:
            plug->result = a;
            break;
        case NOT:
            plug->result = ~b;
            break;
        case AND:
            plug->result = a & b;
            break;
        case OR:
            plug->result = a | b;
            break;
        case LSHIFT:
            plug->result = a << b;
            break;
        case RSHIFT:
            plug->result = a >> b;
            break;
    }
    plug->resolved = true;
    return plug->result;
}

void main() {
    const size_t domain = alpha_range * (alpha_range + 1);
    connector *plugs[domain];
    connector *plug = new_connector();
    char token[8] = "";
    for(short step = 0; EOF != scanf("%s", token); ++step) {
        if (match(token, "->")) step = 3;
        switch (step) {
            case 0:
                if (!match(token, "NOT")) {
                    read_parameter(token, &plug->in[0]);
                    break;
                }
                ++step;
            case 1:
                plug->gate = read_gate(token);
                break;
            case 2:
                read_parameter(token, &plug->in[1]);
                break;
            case 3:
                break;
            case 4:
                strcpy(plug->out, token);
                plugs[_(plug->out)] = plug;
                plug = new_connector();
                step = -1;
                break;
        }
    }
    printf("%i\n", compute(plugs, "a"));
}
