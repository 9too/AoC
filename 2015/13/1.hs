import Data.List
import qualified Data.Map.Strict as Map

type Person = String
type Happiness = Int
type Bible = Map.Map Person (Map.Map Person Happiness)

parse :: String -> [(Person,(Person,Happiness))]
parse input = let transform (x:"would":"gain":happiness:xs) = (x, (last xs,          read happiness))
                  transform (x:"would":"lose":happiness:xs) = (x, (last xs, negate $ read happiness))
                  transform invalid = error $ show invalid
               in map (transform . words . init) $ lines input

index :: [Person] -> [(Person, (Person, Happiness))] -> Bible
index list preferences = let regroup x = (x, Map.fromList $ map snd $ filter ((x==).fst) preferences)
                          in Map.fromList $ map regroup list

dinner :: Bible -> [Person] -> Happiness
dinner _ [_] = 0
dinner bible (person:left) = let neighbor = head left
                              in (Map.! neighbor) bible Map.! person + dinner bible left

seats :: [a] -> [a]
seats list = list ++ [head list] ++ reverse list

main :: IO ()
main = do
    input <- getContents
    let preferences = parse input
        list = nub $ concatMap (\(x,(y,_)) -> [x,y]) preferences
        bible = index list preferences
    print
        $ maximum
        $ map (dinner bible) . seats
        $ permutations list
