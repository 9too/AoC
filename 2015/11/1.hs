import Data.Char

inc :: Char -> (Char, Bool)
inc 'z' = ('a', True)
inc  x  = (chr (ord x + 1), False)

next :: String -> (String, Bool)
next [x]    = (\(char,carry) -> ([char],carry)) $ inc x
next (x:xs) | carry     = let (char,bump) = inc x
                           in (char:trail,bump)
            | otherwise = (x:trail, False)
            where (trail,carry) = next xs
next _ = ([],False)

two2 :: String -> Bool
two2 string = let a = string
                  b = tail a
                  eq = uncurry (==)
               in any eq
                  $ (\x -> case x of _:_:xs -> xs; _ -> [])
                  $ dropWhile (not.eq)
                  $ zip a b

three :: String -> Bool
three string = let a = string
                   b = tail a
                   c = tail b
                in any
                   (\(x,y,z) -> all (== ord x) [ord y - 1, ord z - 2])
                   $ zip3 a b c

iol :: String -> Bool
iol = any (`elem` "iol")

main :: IO ()
main = do
    input <- getContents
    let try = fst . next
        predicates = [two2,three,not.iol]
     in putStrLn
     $ until (\string -> all ($string) predicates) try
     $ try
     $ filter (/= '\n') input
