#include <stdio.h>
#include <ctype.h>

void main() {
    unsigned int lit = 0, rep = 0;
    char glyph;
    while (EOF != (glyph = getchar())) {
        if (!isspace(glyph)) {
            ++rep;
            if ('"' != glyph) ++lit;
            if ('\\' == glyph) {
                ++rep;
                if ('x' == getchar()) {
                    rep += 2;
                    getchar(); getchar();
                }
            }
        }
    }
    printf("%i\n", rep - lit);
}
