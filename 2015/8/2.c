#include <stdio.h>
#include <ctype.h>

void main() {
    unsigned int lit = 0, rep = 0;
    char glyph;
    while (EOF != (glyph = getchar())) {
        if (!isspace(glyph)) {
            ++lit;
            ++rep;
            if ('"' == glyph)
                rep += 2;
            if ('\\' == glyph) {
                ++rep;
                glyph = getchar();
                if ('"' == glyph || '\\' == glyph)
                    ++rep;
                ++rep;
                ++lit;
            }
        }
    }
    printf("%i\n", rep - lit);
}
