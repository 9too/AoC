import Data.Char

clean :: Char -> Char
clean x 
  | isNumber x = x
  | x == '-'   = x
  | otherwise  = ' '

toNumber :: String -> Int
toNumber = read

main :: IO ()
main = do
    input <- getContents
    print 
     $ sum
     $ map toNumber 
     $ words 
     $ map clean input
