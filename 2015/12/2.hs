import Control.Applicative

data Type = Object | Array deriving (Show)


scan :: Type -> String -> (Maybe String,String)

scan Object ('r':'e':'d':s) = (Nothing, snd $ scan Object s)

scan Array  (']':s) = (Just "", s)
scan Object ('}':s) = (Just "", s)

scan t ('[':s) = sub s t Array
scan t ('{':s) = sub s t Object

scan t (c:s) = let (x,y) = scan t s
                in (fmap (c:) x, y)

scan _ "" = (Just "","")


sub :: String -> Type -> Type -> (Maybe String,String)
sub s t t' = let (nest,left)  = scan t' s
                 (flat,left') = scan t left
              in (concat <$> sequence [nest <|> Just "",flat], left')


scanner :: String -> Maybe String
scanner = fst . scan Array

main :: IO ()
main = do
    input <- getContents
    print $ scanner input
