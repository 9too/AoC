#include <stdio.h>

#define X 1000
#define Y 1000

typedef struct { int x,y; } Position;
typedef char World[X][Y];

void main() {
    World map = {0};
    Position p = {X/2,Y/2};
    int count = 0;
    char c;
    map[p.y][p.x] = 1;
    while(EOF != (c = getchar())) {
        switch(c) {
            case '^': p.y++; break;
            case 'v': p.y--; break;
            case '<': p.x--; break;
            case '>': p.x++; break;
        }
        map[p.y][p.x] = 1;
    }
    for (int j=0; j<Y; ++j)
        for (int i=0; i<X; ++i)
            count += map[j][i];
    printf("%i\n",count);
}
