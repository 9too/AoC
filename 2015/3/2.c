#include <stdio.h>

#define X 1000
#define Y 1000

typedef struct { int x,y; } Position;
typedef char World[X][Y];

void main() {
    World map = {0};
    Position p[2] = {{X/2,Y/2},{X/2,Y/2}};
    int n = 0, count = 0;
    char c;
    map[p[0].x][p[0].y] = 1;
    while(EOF != (c = getchar())) {
        switch(c) {
            case '^': p[n].y++; break;
            case 'v': p[n].y--; break;
            case '<': p[n].x--; break;
            case '>': p[n].x++; break;
        }
        map[p[n].y][p[n].x] = 1;
        n = (n + 1) % 2;
    }
    for (int j=0; j<Y; ++j)
        for (int i=0; i<X; ++i)
            count += map[j][i];
    printf("%i\n",count);
}
