#include <stdio.h>
#include <string.h>

int three_vowels(const char * const word) {
    int i, vowels = 0;
    for (i=0; i<16; ++i)
        if (strchr("aeiou", word[i]))
            if (++vowels >= 3)
                return 1;
    return 0;
}

int double_letter(const char * const word) {
    int i;
    for (i=1; i<16; ++i)
        if (word[i] == word[i-1])
            return 1;
    return 0;
}

int naughty(const char * const word) {
    return strstr(word,"ab") ||
           strstr(word,"cd") ||
           strstr(word,"pq") ||
           strstr(word,"xy");
}

void main() {
    char word[16];
    int nice = 0;
    while (EOF != scanf("%s",word))
        nice += three_vowels(word) && double_letter(word) && !naughty(word);
    printf("%i\n",nice);
}
