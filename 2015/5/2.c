#include <stdio.h>
#include <string.h>

int double_double(char *word) {
    char pair[] = "00";
    for (word; *(word+2)!='\0'; ++word) {
        pair[0] = *word;
        pair[1] = *(word+1);
        if (NULL != strstr((word+2),pair))
            return 1;
    }
    return 0;
}

int double_letter(char *word) {
    for (word+=2; *word!='\0'; ++word)
        if (*word == *(word-2))
            return 1;
    return 0;
}

void main() {
    int nice = 0;
    char word[32];
    while (EOF != scanf("%s",word))
        nice += double_letter(word) && double_double(word);
    printf("%i\n",nice);
}
