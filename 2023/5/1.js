const fs = require('fs');
const input = fs.readFileSync(process.stdin.fd);
const rows = input.toString().split('\n').filter(row => row.trim());
const seeds = rows[0].split(':')[1].trim().split(' ').map(Number);
const maps = [];
let map = {};
let converter = [];
const pipeline = [];
for (row of rows.slice(1)) {
    if (row.includes('map')) {
        [label] = row.split(' ');
        [from, _, to] = label.split('-');
        converter = [];
        map = { from, to, converter };
        maps.push(map);
    } else {
        converter.push(row.split(' ').map(Number));
    }
}
console.log(maps);
for (let cursor='location'; cursor!='seed'; cursor=map.from) {
    console.log(cursor);
    map = maps.find(({to}) => to==cursor);
    pipeline.push(maps.indexOf(map));
}
pipeline.reverse();
function validate(value, [destination, source, range]) {
    if ( source <= value && value < source + range) {
        return value + (destination - source);
    }
}
function convert(value, converters) {
    for (const converter of converters) {
        const result = validate(value, converter)
        if (result) {
            return result;
        }
    }
    return value;
}
const results={};
for (const seed of seeds) {
    const process = [];
    for (let step of pipeline) {
        process.push(convert(process[step-1] || seed, maps[step].converter));
    }
    results[seed] = process;
}
const locations = Object.values(results).map(process => process.slice(-1)[0]);
console.log(locations.reduce((min, location) => Math.min(min,location)));
