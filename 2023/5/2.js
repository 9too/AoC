const fs = require('fs');
const input = fs.readFileSync(process.stdin.fd);
const rows = input.toString().split('\n').filter(row => row.trim());
const seeds = rows[0].split(':')[1].trim().split(' ').map(Number);
const maps = [];
let lowest = Infinity;
let map = {};
let converter = [];
function valid_seed(seed) {
    for (let i=0; i+1<seeds.length; i+=2) {
        const [n, range] = seeds.slice(i);
        if (n <= seed && seed < n+range) {
            return true;
        }
    }
    return false;
}
function convert(value, converters) {
    for (const [destination, source,  range] of converters) {
        if ( destination <= value && value < destination + range) {
            return value + (source - destination);
        }
    }
    return value;
}
for (row of rows.slice(1)) {
    if (row.includes('map')) {
        [label] = row.split(' ');
        [from, _, to] = label.split('-');
        converter = [];
        map = { from, to, converter };
        maps.push(map);
    } else {
        converter.push(row.split(' ').map(Number));
    }
}

for (let location=0; location<Infinity; ++location) {
    previous = location;
    for (let step=6; step>=0; --step) {
        previous = convert(previous, maps[step].converter);
    }
    if (valid_seed(previous)) {
        console.log(location);
        return;
    }
}
