const fs = require('fs');
const input = fs.readFileSync(process.stdin.fd);
const rows = input.toString().split('\n').filter(row => row.trim());
const isNumber = (n) => (Number.isInteger(parseInt(n)));
const cards = [];
let total=0;
for (const row of rows) {
    const [card, numbers] = row.split(': ');
    const [win, pool] = numbers.split(' | ');
    const [,id] = card.split(/ +/);
    cards.push({
        id: Number(id)-1,
        win: win.trim().split(/ +/).map(Number).sort(),
        pool: pool.trim().split(/ +/).map(Number).sort(),
    });
}
for (let index=0; index<cards.length; index++) {
    const card = cards[index];
    card.won = card.pool.filter(number => card.win.includes(number));
    card.gives = [];
    for (let give=1; give<=card.won.length && cards[index+give] ; ++give) {
        card.gives.push(index+give);
    }
}
const visit = [...Array(cards.length).keys()].reverse();
const visited = [];
while (visit.length) {
    const next = visit.pop();
    visited.push(next);
    visit.push(...cards[next].gives);
}
console.log(visited.length);
