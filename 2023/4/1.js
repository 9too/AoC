fs = require('fs');
input = fs.readFileSync(process.stdin.fd);
rows = input.toString().split('\n').filter(row => row.trim());
isNumber = (n) => (Number.isInteger(parseInt(n)));
cards = [];
total = 0;
for (row of rows) {
    [card, numbers] = row.split(': ');
    [win, pool] = numbers.split(' | ');
    [,id] = card.split(/ +/);
    cards.push({
        card: Number(id),
        win: win.trim().split(/ +/).map(Number),
        pool: pool.trim().split(/ +/).map(Number),
    });
}
for ({card,win,pool} of cards) {
    valid = pool.filter(n => win.includes(n));
    if (valid.length) {
        total += Math.pow(2, valid.length-1);
    }
}
console.log(total);
