const fs = require('fs');
const input = fs.readFileSync(process.stdin.fd);
const rows = input.toString().trim().split('\n');
const values = ['J','2','3','4','5','6','7','8','9','T','Q','K','A'];
const five  = (hand) => values.filter(value => 5 == hand.filter(card => [value, 'J'].includes(card)).length);
const four  = (hand) => values.filter(value => 4 == hand.filter(card => [value, 'J'].includes(card)).length);
const three = (hand) => values.filter(value => 3 == hand.filter(card => value==card).length);
const two   = (hand) => values.filter(value => 2 == hand.filter(card => value==card).length);
const joker = (hand) => hand.filter(value => value=='J').length;
const hands = rows.map(row => {
    const [cards, bid] = row.split(/ +/)
    const hand = {
        cards: cards.split(''),
        bid: Number(bid),
        rank: '0',
    };
    // pair
    if (
        (two(hand.cards).length)
        ||
        (joker(hand.cards)==1)
    )
        hand.rank = '1';
    // double pair
    if (
        (two(hand.cards).length == 2)
        || 
        (two(hand.cards).length == 1 && joker(hand.cards) == 1)
    )
        hand.rank = '2';
    // triple
    if (
        (three(hand.cards).length)
        || 
        (two(hand.cards).length && joker(hand.cards) == 1)
        ||
        (joker(hand.cards) == 2)
    )
        hand.rank = '3';
    // fullhouse
    if (
        (three(hand.cards).length && two(hand.cards).length)
        ||
        (two(hand.cards).length == 2 && joker(hand.cards) == 1)
    )
        hand.rank = '4';
    // four
    if (four(hand.cards).length)
        hand.rank = '5';
    // five
    if (five(hand.cards).length)
        hand.rank = '6';
    hand.rank = hand.rank + hand.cards.map(card => values.indexOf(card).toString(16)).join('')
    return hand;
});
hands.sort((a,b) => {
    if (a.rank>b.rank) {
        return 1;
    }
    if (b.rank>a.rank) {
        return -1;
    }
    return 0;
});
let total = 0;
for (let rank=0; rank<hands.length; ++rank) {
    const result = (rank+1) * hands[rank].bid;
    total += result;
}
console.log(total);
