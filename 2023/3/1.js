fs = require('fs');
input = fs.readFileSync(process.stdin.fd);
rows = input.toString().split('\n').filter(row => row.trim());
isNumber = (n) => (Number.isInteger(parseInt(n)));
isSymbol = (s) => (s && !isNumber(s) && s != '.');
total = 0;
numbers = [];
function verify(rows, match, x) {
	y = match.index;
	for (dx of [-1,0,1]) {
		for (dy=-1; dy<=match.number.length; dy++) {
			symbol = rows[x+dx]?.[y+dy];
			if (isSymbol(symbol)) {
				return true;
			}
		}
	}
}
for (row of rows) {
	numbers.push(
		Array.from(
			row.matchAll(/[0-9]+/g),
		 	(match) => ({
				number: match[0],
				index: match.index
			})
		)
	);
}
for (x=0; x<rows.length; ++x) {
	for (match of numbers[x]) {
		if (verify(rows, match, x)) {
			total += Number(match.number);
		}
	}
}
console.log(total);
