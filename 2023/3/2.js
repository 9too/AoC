fs = require('fs');
input = fs.readFileSync(process.stdin.fd);
rows = input.toString().split('\n').filter(row => row.trim());
isNumber = (n) => (Number.isInteger(parseInt(n)));
isSymbol = (s) => (s == '*');
overlap = (x1,x2,y1,y2) => x1 <= y2 && y1 <= x2;
total = 0;
numbers = [];
gears = [];
for (x=0; x<rows.length; ++x) {
    gears.push(
	    ...Array.from(
			rows[x].matchAll(/\*/g),
		 	({index}) => (index)
		).filter(y => {
            n=0;
            n+=isNumber(rows[x][y-1]);
            n+=isNumber(rows[x][y+1]);
            n+=rows[x-1]?.slice(y-1,y+2).split(/[^0-9]/).filter(isNumber).length;
            n+=rows[x+1]?.slice(y-1,y+2).split(/[^0-9]/).filter(isNumber).length;
            return n == 2;
        }).map(y => ({x,y}))
    );
	numbers.push(
		Array.from(
			rows[x].matchAll(/[0-9]+/g),
		 	(match) => ({
				number: match[0],
				index: match.index
			})
		)
	);
}
for ({x,y} of gears) {
    multiply = [];
    search = [...numbers[x-1],...numbers[x],...numbers[x+1]];
    for ({number,index} of search) {
        if (overlap(y-1,y+1,index,index+number.length-1)) {
            multiply.push(Number(number));
        }
    }
    [first,second] = multiply;
    total += first*second;
}
console.log(total);
