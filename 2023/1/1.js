fs = require('fs');
input = fs.readFileSync(process.stdin.fd);
rows = input.toString().split('\n');
total = 0;
for (row of rows) {
    if (row) {
        numbers = row.replace(/[^0-9]/g, '');
        first = numbers.slice(0,1);
        last = numbers.slice(-1);
        total += Number(first + last);
    }
}
console.log(total);
