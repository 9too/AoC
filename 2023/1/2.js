fs = require('fs');
input = fs.readFileSync(process.stdin.fd);
rows = input.toString().split('\n');
total = 0;
digits = [ 'zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine' ];
// positive lookahead hack
regex = new RegExp(`(?=([0-9]|${digits.join('|')}))`, 'g');
for (row of rows) {
    if (row){
        match = Array.from(row.matchAll(regex), x => x[1]);
        [first] = match.slice(0,1);
        [last] = match.slice(-1);
        number = String(Number(first) ? first : digits.indexOf(first))
               + String(Number(last)  ? last  : digits.indexOf(last));
        total += Number(number);
    }
}
console.log(total);
