const fs = require('fs');
const input = fs.readFileSync(process.stdin.fd);
const rows = input.toString().split('\n');
const times = rows[0].split(':')[1].trim().split(/ +/).map(Number);
const distances = rows[1].split(':')[1].trim().split(/ +/).map(Number);
const simulations = [];
const better = [];
for (let scenario=0; scenario<times.length; ++scenario) {
    const simulation = [];
    const time = times[scenario];
    const distance = distances[scenario];
    for (let push=0; push<=time; ++push) {
        simulation[push] = (time-push)*push;
    }
    simulations[scenario] = simulation;
    better[scenario] = simulation.filter(result => result>distance).length;
}
console.log(better.reduce((total, result)=>total*result));

