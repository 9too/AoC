const fs = require('fs');
const input = fs.readFileSync(process.stdin.fd);
const rows = input.toString().split('\n');
const time = Number(rows[0].split(':')[1].trim().replace(/ +/g,''));
const distance = Number(rows[1].split(':')[1].trim().replace(/ +/g, ''));
let total = 0;
for (let push=0; push<=time; ++push) {
    if((time-push)*push>distance) {
        ++total;
    }
}
console.log(total);

