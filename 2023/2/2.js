fs = require('fs');
input = fs.readFileSync(process.stdin.fd);
rows = input.toString().split('\n').filter(row => row.trim());
games = [];
regex = new RegExp(`Game ([0-9]+): (.*)`);
for (row of rows) {
    match = row.match(regex);
    no = Number(match[1]);
    rounds = match[2]
        .split('; ')
        .map(round => round
            .split(', ')
            .reduce((acc, hand) => {
                [number, color] = hand.split(' ');
                acc[color] = Number(number);
                return acc;
            }, {})
        );
    games.push({no, rounds});
}
total = 0;
for ({rounds} of games) {
    min = { red: 0, green: 0, blue: 0 };
    for (round of rounds) {
        for (color in min) {
            if (round[color] > min[color]) {
                min[color] = round[color];
            }
        }
    }
    total += min.red * min.green * min.blue;
}
console.log(total);
