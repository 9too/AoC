fs = require('fs');
input = fs.readFileSync(process.stdin.fd);
rows = input.toString().split('\n').filter(row => row.trim());
games = [];
regex = new RegExp(`Game ([0-9]+): (.*)`);
for (row of rows) {
    match = row.match(regex);
    no = Number(match[1]);
    rounds = match[2]
        .split('; ')
        .map(round => round
            .split(', ')
            .reduce((acc, hand) => {
                [number, color] = hand.split(' ');
                acc[color] = Number(number);
                return acc;
            }, {})
        );
    games.push({no, rounds});
}
restrictions = { red: 12, green: 13, blue: 14 };
valid_games = [];
for ({no, rounds} of games) {
    valid = true;
    for (round of rounds) {
        for (color in restrictions) {
            if (round[color] > restrictions[color]) {
                valid = false;
            }
        }
    }
    if (valid) {
        valid_games.push(no);
    }
}
console.log(valid_games);
console.log(valid_games.reduce((sum, value) => sum + value, 0));
