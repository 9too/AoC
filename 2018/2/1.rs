use std::io;
use std::io::prelude::*;
use std::collections::HashMap;
use std::collections::HashSet;
use std::iter::FromIterator;

fn main() {
    let mut twos = 0;
    let mut threes = 0;
    let mut map: HashMap<char, u64> = HashMap::with_capacity(24);
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        for c in line.unwrap().chars() {
            *(map.entry(c).or_insert(0)) += 1;
        }
        {
            let counts: HashSet<&u64> = HashSet::from_iter(map.values());
            if counts.contains(&2) {
                twos += 1;
            }
            if counts.contains(&3) {
                threes += 1;
            }
        }
        map.clear();
    }
    println!("{}", twos * threes);
}
