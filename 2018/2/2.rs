use std::io;
use std::io::prelude::*;

fn read_input() -> Vec<String> {
    let mut vec = Vec::new();
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        vec.push(line.unwrap());
    }
    return vec;
}

fn strip_diff(s_1: &String, s_2: &String) -> String {
    let mut res = String::new();
    for (i, c_1) in s_1.chars().enumerate() {
        match s_2.chars().nth(i) {
            Some(c_2) => if c_1 == c_2 { res.push(c_1) },
            _ => (),
        }
    }
    return res;
}

fn main() {
    let vec = read_input();
    for code_1 in vec.iter() {
        for code_2 in vec.iter() {
            let (mut it_1, mut it_2) = (code_1.chars(), code_2.chars());
            let mut diff = 0;
            loop {
                match (it_1.next(), it_2.next()) {
                    (Some(v1), Some(v2)) => diff += if v1 == v2 { 0 } else { 1 },
                    _ => break,
                }
            }
            if diff == 1 {
                println!("{}", strip_diff(code_1, code_2));
                return;
            }
        }
    }
}
