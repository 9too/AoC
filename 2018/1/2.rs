use std::io;
use std::io::prelude::*;
use std::collections::HashSet;
use std::vec::Vec;

fn main() {
    let mut acc = 0;
    let mut set: HashSet<i64> = HashSet::new();
    let mut vec = Vec::new();
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        vec.push(line.unwrap().parse::<i64>().unwrap());
    }
    'search: loop {
        for n in &vec {
            acc += n;
            if set.contains(&acc) {
                break 'search;
            }
            set.insert(acc);
        }
    }
    println!("{}", acc);
}
