use std::io;
use std::io::prelude::*;

fn main() {
    let mut x = 0;
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        x += line.unwrap().parse::<i64>().unwrap();
    }
    println!("{}", x);
}
