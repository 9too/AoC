use std::io;
use std::io::prelude::*;

struct Position {
    x: u16,
    y: u16,
}

struct Area {
    w: u16,
    h: u16,
}

struct Claim {
    id: u16,
    pos: Position,
    area: Area,
}

fn read_id(claim: &String) -> u16 {
    let section = String::from(claim.split(" ").nth(0).unwrap());
    return section.get(1..).unwrap().parse::<u16>().unwrap();
}

fn read_pos(claim: &String) -> Position {
    let mut section = String::from(claim.split(" ").nth(2).unwrap());
    section.pop(); // remove ":"
    let mut values = section.split(",");
    let mut extract = || values.next().unwrap().parse::<u16>().unwrap();
    return Position { x: extract(), y: extract() };
}

fn read_area(claim: &String) -> Area {
    let section = String::from(claim.split(" ").nth(3).unwrap());
    let mut values = section.split("x");
    let mut extract = || values.next().unwrap().parse::<u16>().unwrap();
    return Area { w: extract(), h: extract() };
}

fn parse_claim(claim: &Claim, f: &mut FnMut(usize, usize)) {
    for i in claim.pos.x .. claim.pos.x + claim.area.w {
        for j in claim.pos.y .. claim.pos.y + claim.area.h {
            f(i as usize, j as usize);
        }
    }
}

fn main() {
    let mut matrix: [[u8; 1000]; 1000] = [[0; 1000]; 1000];
    let mut claims = Vec::new();
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let string = line.unwrap();
        claims.push(Claim {
            id: read_id(&string),
            pos: read_pos(&string),
            area: read_area(&string)
        });
    }
    for claim in &claims {
        parse_claim(claim, &mut |i,j| matrix[i][j] += 1);
    }
    for claim in &claims {
        let mut overlap = false;
        parse_claim(claim, &mut |i,j| if matrix[i][j] > 1 { overlap = true; });
        if !overlap {
            println!("{}", claim.id);
        }
    }
}
