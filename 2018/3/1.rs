use std::io;
use std::io::prelude::*;

fn read_pos(claim: &String) -> (u16,u16) {
    let mut section = String::from(claim.split(" ").nth(2).unwrap());
    section.pop(); // remove ":"
    let mut values = section.split(",");
    let mut extract = || values.next().unwrap().parse::<u16>().unwrap();
    return (extract(), extract());
}

fn read_area(claim: &String) -> (u16,u16) {
    let section = String::from(claim.split(" ").nth(3).unwrap());
    let mut values = section.split("x");
    let mut extract = || values.next().unwrap().parse::<u16>().unwrap();
    return (extract(), extract());
}

fn main() {
    let mut count = 0;
    let mut matrix = [[0; 1000]; 1000];
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let string = line.unwrap();
        let (x, y) = read_pos(&string);
        let (w, h) = read_area(&string);
        for i in x..x+w {
            for j in y..y+h {
                matrix[i as usize][j as usize] += 1;
            }
        }
    }
    for row in matrix.iter() {
        for col in row.iter() {
            if col > &1 {
                count += 1;
            }
        }
    }
    println!("{}", count);
}
