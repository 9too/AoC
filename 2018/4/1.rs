use std::io;
use std::io::prelude::*;

#[derive(Debug)]
struct Stamp {
    year: u16,
    month: u8,
    day: u8,
    hour: u8,
    minute :u8
}

#[derive(Debug)]
enum Log {
    Asleep, Awake, Shift(u16)
}

fn read_stamp(entry: &String) -> Stamp {
    let section = String::from(entry.split("]").next().unwrap());
    let mut datetime = section.get(1..).unwrap().split(" ");
    let mut date = datetime.next().unwrap().split("-");
    let mut time = datetime.next().unwrap().split(":");
    return Stamp {
        year   : date.next().unwrap().parse::<u16>().unwrap(),
        month  : date.next().unwrap().parse::<u8>().unwrap(),
        day    : date.next().unwrap().parse::<u8>().unwrap(),
        hour   : time.next().unwrap().parse::<u8>().unwrap(),
        minute : time.next().unwrap().parse::<u8>().unwrap()
    };
}

fn read_log(entry: &String) -> Log {
    let section = String::from(entry.split(" ").nth(3).unwrap());
    return 
        if section.contains("up") { Log::Awake }
        else if section.contains("asleep") { Log::Asleep }
        else { Log::Shift(section.get(1..).unwrap().parse::<u16>().unwrap()) }
}

fn main() {
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let entry = line.unwrap();
        let stamp = read_stamp(&entry);
        let log = read_log(&entry);
        println!("{:?} {:?}", stamp, log);
    }
}
