#include <stdio.h>

void main() {
    int v[3], count=0;
    while (EOF != scanf("%i %i %i",&v[0],&v[1],&v[2]))
        if (v[0]>=v[1] && v[0]>=v[2] && v[0]<v[1]+v[2] ||
            v[1]>=v[0] && v[1]>=v[2] && v[1]<v[0]+v[2] ||
            v[2]>=v[0] && v[2]>=v[1] && v[2]<v[0]+v[1])
            ++count;
    printf("%i\n",count);
}
