#include <stdio.h>

int pop(const int q[3]) {
    int valid = 0;
    if (q[0]>=q[1] && q[0]>=q[2] && q[0]<q[1]+q[2] ||
        q[1]>=q[0] && q[1]>=q[2] && q[1]<q[0]+q[2] ||
        q[2]>=q[0] && q[2]>=q[1] && q[2]<q[0]+q[1])
        valid = 1;
    return valid;
}

void main() {
    int v[3], q[3][3], i=0, count=0;
    while (EOF != scanf("%i %i %i",&v[0],&v[1],&v[2])) {
        q[0][i] = v[0]; q[1][i] = v[1]; q[2][i] = v[2];
        if (++i == 3) {
            count += pop(q[0]) + pop(q[1]) + pop(q[2]);
            i = 0;
        }
    }
    printf("%i\n",count);
}
