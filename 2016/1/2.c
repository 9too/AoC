#include <stdio.h>

#define X 1000
#define Y 1000

typedef enum { N, W, S, E } Direction;
typedef struct { int x,y; } Position;
typedef char World[X][Y];

int get_distance() {
    int steps;
    scanf("%i",&steps);
    return steps;
}

Direction turn_left(Direction direction) {
    return (direction + 1) % 4;
}

Direction turn_right(Direction direction) {
    return (direction + 3) % 4;
}

char visit(World *map, const Position p) {
    const char visited = (*map)[p.x][p.y];
    (*map)[p.x][p.y] = 1;
    return visited;
}

char advance(World *map, Position *from, Direction to) {
    const int distance = get_distance(),
              x = from->x,
              y = from->y;
    char found = 0;
    switch(to) {
        case N:
            while ( ++from->y < y+distance )
                if ( found = visit(map, *from) )
                    break;
            break;
        case W:
            while ( --from->x > x-distance )
                if ( found = visit(map, *from) )
                    break;
            break;
        case S:
            while ( --from->y > y-distance )
                if ( found = visit(map, *from) )
                    break;
            break;
        case E:
            while ( ++from->x < x+distance )
                if ( found = visit(map, *from) )
                    break;
            break;
    }
    return found;
}

void main() {
    World map = {0};
    Direction to = N;
    Position p = {X/2,Y/2};
    while(1) {
        switch(getchar()) {
            case 'L': to = turn_left(to);  break;
            case 'R': to = turn_right(to); break;
            case EOF: return;
            default:  continue;
        }
        if ( advance(&map, &p, to) ) {
            printf("%i,%i\n", p.x-X/2, p.y-Y/2);
            return;
        }
    }
}
