#include <stdio.h>

int read_steps() {
    int steps;
    scanf("%i",&steps);
    return steps;
}

int result(int distance[4]) {
    printf("%i\n", (distance[0]-distance[2])+(distance[3]-distance[1]));
    return 0;
}

int turn_left(char direction) {
    return (direction + 1) % 4;
}

int turn_right(char direction) {
    return (direction + 3) % 4;
}

int main(int argc, char *argv[]) {
    // N:0 - E:1 - S:2 - W:3
    char direction = 0;
    int distance[4] = {0,0,0,0};
    int steps;
    while(1) {
        switch(getchar()) {
            case 'L': direction = turn_left(direction); break;
            case 'R': direction = turn_right(direction); break;
            case EOF: return result(distance);
            default: continue;
        }
        distance[direction] += read_steps();
    }
}
