#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <bsd/md5.h>

void main() {
    unsigned int n = 0, i = 0;
    char in[64], out[64];
    scanf("%s",in);
    while (++n) {
        sprintf(out,"%s%i",in,n);
        char *sum = MD5Data(out, strlen(out), NULL);
        if (!strncmp(sum,"00000",5)) {
            putchar(sum[5]);
            if (++i==8) break;
        }
        free(sum);
    }
    putchar('\n');
}
