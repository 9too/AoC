#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <bsd/md5.h>

void main() {
    int n=0, i=0;
    char in[64], out[64], *x,
         password[]="        ";
    setbuf(stdout,NULL);
    scanf("%s",in);
    while (1) {
        sprintf(out,"%s%i",in,n);
        char *sum = MD5Data(out, strlen(out), NULL);
        if (!strncmp(sum,"00000",5) && '0' <= sum[5] && sum[5] < '8') {
            x = password + sum[5] - '0';
            if (*x == ' ') {
                *x = sum[6];
                printf("\r%s",password);
                if (++i==8) {
                    putchar('\n');
                    break;
                }
            }
        }
        free(sum);
        ++n;
    }
}
