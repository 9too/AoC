#include <stdio.h>
#include <string.h>

void check(int *count, char *code) {
    char c;
    for (c = *code; c != '\0'; c = *(++code))
        if ('a'<=c && c<='z')
            count[c]++;
}

void frequency(int *count, char *frequent) {
    char c, *f, *replace, *end = frequent;
    for (c='a'; c<='z'; ++c) {
        for (f=frequent; f!=end; ++f)
            if (count[c] > count[*f]) {
                for (replace=end; replace!=f; --replace)
                    *replace = *(replace-1);
                *f = c;
                break;
            }
        if (end != frequent+5) {
            if (f == end)
                *f = c;
            ++end;
        }
    }
}

void decipher(char *code, int room) {
    char c;
    printf("%i - ",room);
    for (c=*code; c!='\0'; c=*(++code))
        if (c == '-')
            putchar(' ');
        else
            putchar('a'+(((c-'a')+room)%26));
    putchar('\n');
}

void main() {
    int room, total = 0, count['z'+1];
    char line[64], code[64], checksum[5], frequent[5];
    while (EOF != scanf("%s",line)) {
        int split = strrchr(line,'-') - line;
        memset(count,0,sizeof(count));
        memcpy(code,line,split);
        code[split] = '\0';
        sscanf(line+split,"-%i[%5c]",&room,checksum);
        check(count, code);
        frequency(count, frequent);
        if (!strncmp(frequent,checksum,5))
            decipher(code, room);
    }
}
