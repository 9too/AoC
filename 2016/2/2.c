#include <stdio.h>

typedef char Keypad[5][5];
typedef struct { int x,y; } Position;

void main() {
    Keypad pad = {{ 0 , 0 ,'1', 0 , 0 },
                  { 0 ,'2','3','4', 0 },
                  {'5','6','7','8','9'},
                  { 0 ,'A','B','C', 0 },
                  { 0 , 0 ,'D', 0 , 0 }};
    Position p = {0,2};
    while(1) {
        switch(getchar()) {
            case 'U':
                if (p.y > 0 && pad[p.y-1][p.x]) p.y--;
                break;
            case 'L':
                if (p.x > 0 && pad[p.y][p.x-1]) p.x--;
                break;
            case 'D':
                if (p.y < 4 && pad[p.y+1][p.x]) p.y++;
                break;
            case 'R':
                if (p.x < 4 && pad[p.y][p.x+1]) p.x++;
                break;
            case '\n':
                putchar(pad[p.y][p.x]);
                break;
            case EOF:
                putchar('\n');
                return;
        }
    }
}
