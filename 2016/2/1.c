#include <stdio.h>

typedef char Keypad[3][3];
typedef struct { int x,y; } Position;

void main() {
    Keypad pad = {{'1','2','3'},
                  {'4','5','6'},
                  {'7','8','9'}};
    Position p = {1,1};
    while(1) {
        switch(getchar()) {
            case 'U':
                if (p.y > 0) p.y--;
                break;
            case 'L':
                if (p.x > 0) p.x--;
                break;
            case 'D':
                if (p.y < 2) p.y++;
                break;
            case 'R':
                if (p.x < 2) p.x++;
                break;
            case '\n':
                putchar(pad[p.y][p.x]);
                break;
            case EOF:
                putchar('\n');
                return;
        }
    }
}
