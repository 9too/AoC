mirror :: Int -> String -> Bool
mirror n list = let l = length list
                 in list !! n == list !! ((n + quot l 2) `mod` l)


parse :: String -> Int -> Int
parse _ 0 = 0
parse input n 
  | mirror n input = read [input !! n] + continue
  | otherwise = continue
  where continue = parse input (n - 1) 

solve :: String -> Int
solve input = parse input $ length input - 1

main :: IO ()
main = do
    input <- getContents
    print $ solve $ filter (/='\n') input
