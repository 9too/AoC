ends :: String -> Int
ends (x:xs) | x == last xs = read [x]
ends _ = 0

parse :: String -> Int
parse (x:y:s)
  | x == y    = read [x] + parse (y:s)
  | otherwise = parse (y:s)
parse _       = 0

solve :: String -> Int
solve input = parse input + ends input

main :: IO ()
main = do
    input <- getContents
    print $ solve $ filter (/='\n') input
