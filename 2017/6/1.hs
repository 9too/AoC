import Data.Maybe
import Data.List

distribute :: Int -> Int -> [Int] -> [Int]
distribute _ _ [] = []
distribute _ 0 xs = xs 
distribute p n (x:xs) = v + o + x : distribute (p-1) (n-o-v) xs
                        where l = length (x:xs)
                              o = quot n (l+1)
                              v | p >= 0 = if (n `mod` l) - (l-p-1) > 0 then 1 else 0
                                | otherwise = 1

routine :: [Int] -> [Int]
routine l = let p = fromJust $ elemIndex (maximum l) l
                (x,y:ys) = splitAt p l
                l' = x ++ [0] ++ ys
             in distribute p y l'

duplicated :: Eq a => [a] -> Bool
duplicated []     = False
duplicated (x:xs) = x `elem` xs

generate :: (a -> a) -> [a] -> [a]
generate _ [] = []
generate f xs = f (head xs) : xs

main :: IO ()
main = do
    input <- getContents
    print 
        $ (\xs -> (length xs - 1, (+) 1 $ fromJust $ elemIndex (head xs) $ tail xs))
        $ until duplicated (generate routine) [ map read $ words input ]
