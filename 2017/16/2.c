#include <stdio.h>
#include <string.h>

#define CODE "abcdefghijklmnop"
#define LEN  (sizeof(CODE) - 1)
#define LOOP 1000000000

enum move {PARTNER, SPIN, EXCHANGE};

typedef struct {
    enum move type;
    union {
        int x[2];
        char p[2];
        int s;
    } args;
} dance;

void partner(char *src, char a, char b) {
    char *pa = strchr(src, a),
         *pb = strchr(src, b); 
    *pa = b;
    *pb = a;
}
void spin(char *src, int spin) {
    char tmp[LEN + 1];
    spin = spin % LEN;
    if (spin != 0) {
        strcpy(tmp, src);
        memcpy(src, &tmp[LEN - spin], spin);
        memcpy(&src[spin], tmp, LEN - spin);
    }
}
void exchange(char *src, int a, int b) {
    char tmp = src[a];
    src[a] = src[b];
    src[b] = tmp;
}

int main() {
    dance list[10000], *it = list, *end;
    int c;
    char src[] = CODE; 
    while (EOF != (c = getchar())) {
        switch(c) {
            case 'p':
                {
                    char a, b;
                    scanf("%c/%c,", &a, &b);
                    *(it++) = (dance) { PARTNER, { .p = {a, b} } };
                }
                break;
            case 's':
                {
                    int s;
                    scanf("%i,", &s);
                    *(it++) = (dance) { SPIN, { .s = s } };
                }
                break;
            case 'x':
                {
                    int a, b;
                    scanf("%i/%i,", &a, &b);
                    *(it++) = (dance) { EXCHANGE, { .x = {a, b} } };
                }
                break;
        }
    }
    end = it;
    for (int count = LOOP; count --> 0;) {
        for (it = list; it < end; ++it) {
            switch (it->type) {
                case PARTNER:
                    partner(src, it->args.p[0], it->args.p[1]);
                    break;
                case SPIN:
                    spin(src, it->args.s);
                    break;
                case EXCHANGE:
                    exchange(src, it->args.x[0], it->args.x[1]);
                    break;
            }
        }
        if (!strcmp(src,CODE)) {
            count = count % (LOOP - count);
        }
    }
    puts(src);
}
