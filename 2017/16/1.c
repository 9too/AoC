#include <stdio.h>
#include <string.h>

void partner(char *src) {
    char a, b, *pa, *pb;
    scanf("%c/%c,", &a, &b);
    pa = strchr(src, a); 
    pb = strchr(src, b); 
    *pa = b;
    *pb = a;
}
void spin(char *src) {
    int spin;
    char *end = src, 
         *dst = src + 1;
    scanf("%i,", &spin);
    while ('\0' != *end) { ++end; }
    --end;
    if (src < end) {
        while (spin --> 0) {
            char last = *end;
            memmove(dst, src, strlen(dst));
            *src = last;
        }
    }
}
void exchange(char *src) {
    char tmp;
    int a, b;
    scanf("%i/%i,", &a, &b);
    tmp = src[a];
    src[a] = src[b];
    src[b] = tmp;
}

int main() {
    char src[] = "abcdefghijklmnop"; 
    int c;
    while (EOF != (c = getchar())) {
        switch(c) {
            case 'p': partner(src);  break;
            case 's': spin(src);     break;
            case 'x': exchange(src); break;
        }
    }
    puts(src);
}
