unique :: Eq a => [a] -> Bool
unique []     = True
unique (x:xs) = x `notElem` xs && unique xs

main :: IO ()
main = do
    input <- getContents
    print $ length $ filter id $ map (unique . words) $ lines input
