import Data.List

anagram :: String -> String -> Bool
anagram x y = sort x == sort y

unique :: [String] -> Bool
unique [_]    = True
unique (x:xs) = not (any (anagram x) xs) && unique xs

main :: IO ()
main = do
    input <- getContents
    print $ length $ filter id $ map (unique . words) $ lines input
