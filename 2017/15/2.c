#include <stdint.h>
#include <stdio.h>

uint64_t generator(uint64_t factor, uint64_t in) {
    return (in * factor) % 2147483647;
}

uint64_t generatorA(uint64_t in) {
    do {
        in = generator(16807, in);
    } while (in % 4);
    return in;
}

uint64_t generatorB(uint64_t in) {
    do {
        in = generator(48271, in);
    } while (in % 8);
    return in;
}

int main() {
    uint64_t count = 0, a = 516, b = 190, mask = 65535;
    for (uint64_t i = 0; i < 5000000; ++i) {
        a = generatorA(a);
        b = generatorB(b);
        if ((a & mask) == (b & mask)) { ++count; }
    }
    printf("%lu\n", count);
}
