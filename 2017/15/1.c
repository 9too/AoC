#include <stdint.h>
#include <stdio.h>

uint64_t generator(uint64_t factor, uint64_t in) {
    return (in * factor) % 2147483647;
}

int main() {
    uint64_t count = 0, gA = 516, gB = 190, mask = 65535;
    for (uint64_t i = 0; i < 40000000; ++i) {
        gA = generator(16807, gA);
        gB = generator(48271, gB);
        if ((gA & mask) == (gB & mask)) { ++count; }
    }
    printf("%lu\n", count);
}
