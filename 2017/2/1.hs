checksum :: [[Int]] -> Int
checksum = foldr (\x -> (+) (maximum x - minimum x)) 0

main :: IO ()
main = do
    input <- getContents
    print $ checksum $ map (map read . words) $ lines input
