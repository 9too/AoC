import Data.List

candidate :: Int -> Int -> Int
candidate x y
  | mod x y == 0 = quot x y
  | mod y x == 0 = quot y x
  | otherwise    = 0

pairs :: [Int] -> [Int]
pairs list = [ candidate x y | (x:ys) <- tails list, y <- ys]

checksum :: [[Int]] -> Int
checksum = foldr ((+) . sum . pairs) 0

main :: IO ()
main = do
    input <- getContents
    print $ checksum $ map (map read . words) $ lines input
