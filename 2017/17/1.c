#include <stdio.h>

#define YEAR 2018

int main() {
    int pos = 0, step = 356, buf[YEAR + 1] = {0};
    for (int i = 1; i < YEAR; ++i) {
        pos = ((pos + step) % i) + 1; 
        for (int x = i; x > pos; --x) {
            buf[x] = buf[x-1];
        }
        buf[pos] = i;
    }
    printf("%i\n", buf[pos+1]);
}
