#include <stdio.h>

#define YEAR 50000000

int main() {
    int ans = -1, pos = 0, step = 356;
    for (int i = 1; i < YEAR; ++i) {
        pos = ((pos + step) % i) + 1; 
        if (1 == pos) {
            ans = i;
        }
    }
    printf("%i\n", ans);
}
