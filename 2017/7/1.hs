import Data.List
import Data.List.Split

parse :: String -> (String, [String])
parse line = let (x:y) = splitOn "->" line
                 [o,_] = words x
              in (o, map (filter (/=',')) $ concatMap words y)

main :: IO ()
main = do
    input <- getContents
    let list = map parse $ lines input
        la = map fst list
        lb = nub $ concatMap snd list
    print $ la \\ lb
