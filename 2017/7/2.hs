import Data.List
import Data.List.Split
import qualified Data.Map.Strict as M

type Tower   = M.Map String [String]
type Weights = M.Map String Int

data Tree = Tree { node   :: String 
                 , weight :: Int
                 , disc   :: [Tree]
                 } deriving (Show)

compose :: Tower -> Weights -> String -> Tree
compose tower weights root 
  = Tree { node   = root
         , weight = weights M.! root
         , disc   = map (compose tower weights) $ tower M.! root
         }

parse :: String -> (Tower, Weights)
parse line 
  = (M.singleton node sub, M.singleton node weight)
  where (from:to) = splitOn ["->"] $ words line
        node = head from
        weight = read $ init $ tail $ last from
        sub = map (filter (/=',')) $ concat to

culprit :: Tree -> [Int]
culprit tree 
  | null $ disc tree          = [weight tree]
  | 1 < length (nub suspects) = suspects
  | otherwise                 = [sum suspects]
  where suspects = concatMap culprit $ disc tree

main :: IO ()
main = do
    input <- getContents
    let (tower, weight) = foldl1 
                          (\(t,w) (t',w') -> (M.union t t', M.union w w')) 
                          $ map parse $ lines input
        nodes = M.keys tower
        elems = nub $ concat $ M.elems tower
        root  = head $ nodes \\ elems
        tree  = compose tower weight root 
    print $ culprit tree
