import Prelude hiding (Left,Right)

data Direction = Up | Down | Left | Right 

up    (x,y) = (x  ,y+1)
down  (x,y) = (x  ,y-1)
left  (x,y) = (x-1,y  )
right (x,y) = (x+1,y  )

find :: Int -> Int -> Int -> (Int,Int)
find number layer low = let high  = 8*layer + low
                            found = number <= high
                         in if found then (layer,low)
                                     else find number (layer + 1) high

follow :: Direction -> Int -> Int -> (Int,Int) -> (Int,Int)
follow _ 0 _ position = position
follow Up   jump layer (x,y) | y ==  layer = follow Left  (jump - 1) layer $ left  (x,y)
follow Left jump layer (x,y) | x == -layer = follow Down  (jump - 1) layer $ down  (x,y)
follow Down jump layer (x,y) | y == -layer = follow Right (jump - 1) layer $ right (x,y)
follow move jump layer position = 
    let next = (jump - 1)
     in case move of 
          Up    -> follow move next layer $ up    position
          Down  -> follow move next layer $ down  position
          Left  -> follow move next layer $ left  position
          Right -> follow move next layer $ right position

main :: IO ()
main = do 
    input <- getContents
    let number = read input
        (layer,low) = find number 0 1
        (x,y) = follow Up (number - (low + 1)) layer (layer,-layer + 1)
     in print $ abs x + abs y
