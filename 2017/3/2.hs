import Data.Maybe
import Data.List

neighbors :: (Int,Int) -> [(Int,Int)]
neighbors (x,y) = [(x-1,y+1),(x,y+1),(x+1,y+1),
                   (x-1,y  ),        (x+1,y  ),
                   (x-1,y-1),(x,y-1),(x+1,y-1)]

linear :: (Int,Int) -> Int
linear (x,y)
  | x ==  n && y >= (-n+1) = i + n + y
  | x <=  n && y ==  n     = i + 2*n + (n - x)
  | x == -n && y <   n     = i + 4*n + (n - y)
  | x >  -n && y == -n     = i' - n + x
  where n = max (abs x) (abs y)
        i  = (2*n - 1) ^ 2
        i' = (2*n + 1) ^ 2

plan :: Int -> (Int,Int)
plan n 
  | n > iii   = (n-iv+layer ,      -layer)
  | n > ii    = (    -layer , iii-n-layer)
  | n > i     = (ii-n-layer ,       layer)
  | otherwise = (     layer ,   n-i+layer)
  where layers = map sum $ tail $ inits (1:[8 * x | x <- [1..]])
        layer  = fromJust $ findIndex (>=n) layers
        side   = 2 * layer
        i   = iv - 3*side
        ii  = iv - 2*side
        iii = iv - 1*side
        iv  = layers !! layer

memoize :: Int -> Int
memoize = (map spirale [0 ..] !!)
    where spirale 1 = 1
          spirale n = sum $ map memoize
                          $ filter (<n) 
                          $ map linear 
                          $ neighbors 
                          $ plan n

main :: IO ()
main = do 
    input <- getContents
    print $ head $ dropWhile (< read input) [ memoize x | x <- [1..]]
