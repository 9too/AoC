#include <stdio.h>

int main() {
    int list[2048], *it = list, *end, jumps = 0;
    while (EOF != scanf("%i",it)) { ++it; }
    end = it;
    it = list;
    while (it < end) {
       int jump = *it; 
       *it += 1;
       it += jump;
       ++jumps;
    }
    printf("%i\n", jumps);
}
