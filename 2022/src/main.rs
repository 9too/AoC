mod day1;
mod day2;

use std::env;
use hyper::{Body, Client, Request};

fn main() {
    println!("Day 1");
    println!(" Part 1 : {}", day1::part1(input(2022, 1)));
    println!(" Part 2 : {}", day1::part2(input(2022, 1)));
}

async fn input(year: u8, day: u8) -> Vec<String> {
    let session = env::var("session").expect("session cookie");
    let client = Client::new();
    let request = Request::builder()
        .uri(format!("https://adventofcode/{year}/day/{day}/input", year = year, day = day))
        .header("Cookie", format!("session={session}", session = session));
    let response = client.request(request).await;
    return response.body.expect("input");
}